#!/usr/bin/env bash
awk -F"\t" '{a[$1]+=($2*$3);}{END{n=asorti(a,sorted);for(i=1; i<=n; i++)print sorted[i], a[sorted[i]];}' file
